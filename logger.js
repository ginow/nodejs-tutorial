const EventEmitter = require('events');
// ES6 syntactic sugar to call constructor
class Logger extends EventEmitter {
    // no need to write 'function' keyword in class
    log(message) {
        console.log(message)
        this.emit('messageLogged')
    }
}
module.exports = Logger;